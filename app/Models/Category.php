<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @author [prasanna]
 * @email [info.prasannam@gmail.com]
 * @create date 2022-03-14 15:04:08
 * @modify date 2022-03-14 15:04:08
 */

class Category extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'category';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the children that owns the category.
     */
    public function children(){
        return $this->hasMany(Category::class, 'parent')->with('children');
    }
}
